package com.legaspi.s01.models;

import javax.persistence.*;

@Entity
@Table(name="comments")
public class Comment {
    // Properties
    // id
    @Id
    @GeneratedValue
    private Long id;

    // content
    @Column (length = 5000)
    private String content;

    // post
    @ManyToOne
    @JoinColumn(name = "post_id", nullable = false)
    private Post post;

    // commenter
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    // Constructor
    public Comment() {}

    public Comment(String content, Post post, User commenter) {
        this.content = content;
        this.post = post;
        this.user = commenter;
    }

    // Getters & Setters

    public Long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User commenter) {
        this.user = commenter;
    }

}
