package com.legaspi.s01.services;

import com.legaspi.s01.models.Comment;
import org.springframework.http.ResponseEntity;

import java.util.Set;

public interface CommentService {
    ResponseEntity<Object> createComment(String token, Long postId, Comment comment);
    ResponseEntity<Object> updateComment(String token, Long postId, Long commentId, Comment comment);
    ResponseEntity<Object> deleteComment(String token, Long postId, Long commentId);
    Set<Comment> getComments(Long postId);
}
