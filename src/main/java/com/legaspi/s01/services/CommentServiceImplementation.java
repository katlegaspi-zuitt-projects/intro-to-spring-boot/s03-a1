package com.legaspi.s01.services;

import com.legaspi.s01.config.JwtToken;
import com.legaspi.s01.models.Comment;
import com.legaspi.s01.models.Post;
import com.legaspi.s01.models.User;
import com.legaspi.s01.repositories.CommentRepository;
import com.legaspi.s01.repositories.PostRepository;
import com.legaspi.s01.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class CommentServiceImplementation implements CommentService{
    @Autowired
    JwtToken jwtToken;

    @Autowired
    private CommentRepository commentRepo;

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private PostRepository postRepo;



    public ResponseEntity<Object> createComment(String token, Long postId, Comment comment) {
        String username = jwtToken.getUsernameFromToken(token);
        User author = userRepo.findByUsername(username);
        Post post = postRepo.findById(postId).get();

        if (post != null) {
            Comment newComment = new Comment(comment.getContent(), post, author);
            commentRepo.save(newComment);

            return new ResponseEntity<>("This comment is now added.", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("This post could be missing or deleted.", HttpStatus.FORBIDDEN);
        }
    }

    public ResponseEntity<Object> updateComment(String token, Long postId, Long commentId, Comment comment) {
        Comment oldComment = commentRepo.findById(commentId).get();
        String username = jwtToken.getUsernameFromToken(token);
        User author = userRepo.findByUsername(username);
        Post post = postRepo.findById(postId).get();

        if (oldComment != null && author.getUsername().equals(username) &&  oldComment.getUser().getUsername().equals(username) && post != null) {
            oldComment.setContent(comment.getContent());
            commentRepo.save(oldComment);

            return new ResponseEntity<>("This comment is now updated.", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("You're not authorized to update this post.", HttpStatus.UNAUTHORIZED);
        }
    }

    public ResponseEntity<Object> deleteComment(String token, Long postId, Long commentId) {
        String username = jwtToken.getUsernameFromToken(token);
        User author = userRepo.findByUsername(username);
        Comment comment = commentRepo.findById(commentId).get();

        if (comment != null && comment.getUser().getUsername().equals(author.getUsername())) {
            commentRepo.delete(comment);

            return new ResponseEntity<>("This comment is now deleted.", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("You're not authorized to delete this comment.", HttpStatus.UNAUTHORIZED);
        }
    }


    public Set<Comment> getComments(Long postId) {
        Post post = postRepo.findById(postId).get();
        return post.getComments();
    }
}
