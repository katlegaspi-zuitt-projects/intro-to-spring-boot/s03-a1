package com.legaspi.s01.services;

import com.legaspi.s01.models.User;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface UserService {
    void createUser(User newUser);
    ResponseEntity updateUser(Long id, User updatedUser, String token);
    ResponseEntity deleteUser(Long id, String token);
    Iterable<User> getUsers(String token);
    Optional<User> findByUsername(String username);
}

