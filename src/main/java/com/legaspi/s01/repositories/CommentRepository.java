package com.legaspi.s01.repositories;

import com.legaspi.s01.models.Comment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentRepository extends CrudRepository<Comment, Object> {
}
